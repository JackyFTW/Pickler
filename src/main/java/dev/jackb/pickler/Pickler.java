/*
 *     Copyright (C) 2023  Jack Barter
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.jackb.pickler;

import dev.jackb.liteprocess.LiteProcess;
import dev.jackb.liteprocess.io.DataStore;
import dev.jackb.liteprocess.io.LogFormat;
import dev.jackb.pickler.events.EventsManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class Pickler extends LiteProcess {

	private static JDA jda;
	private static DataStore config;

	public Pickler() {
		super(LogFormat.EXACT_TIME);
	}

	public static void main(String[] args) throws InterruptedException {
		Pickler process = new Pickler();
		process.addProcessEndListener(Pickler::shutdown);
		process.addInputListener("stop", true, cmd -> System.exit(0));

		config = new DataStore("config", true)
				.addKey("token", String.class, () -> "unset")
				.create();

		startBot();
	}

	private static void startBot() throws InterruptedException {
		System.out.println("Starting bot...");

		jda = JDABuilder.createDefault(config.get("token", String.class), GatewayIntent.getIntents(GatewayIntent.ALL_INTENTS))
				.setMemberCachePolicy(MemberCachePolicy.ALL)
				.enableCache(CacheFlag.getPrivileged())
				.setActivity(Activity.playing("pickleball"))
				.build()
				.awaitReady();

		EventsManager.init();

		System.out.println("Started bot successfully!");
	}

	private static void shutdown() {
		System.out.println("Shutting down...");

		jda.shutdown();

		System.out.println("Shutdown complete!");
	}

	public static Guild getGuild() {
		return jda.getGuildById("1158128213394980986");
	}

}