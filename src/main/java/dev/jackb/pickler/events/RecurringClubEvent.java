/*
 *     Copyright (C) 2023  Jack Barter
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.jackb.pickler.events;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class RecurringClubEvent extends ClubEvent {

	private final LocalTime startTime;
	private final LocalTime endTime;
	private final DayOfWeek dayOfWeek;

	public RecurringClubEvent(String displayName, String location, LocalTime startTime, LocalTime endTime, DayOfWeek dayOfWeek) {
		super(displayName, location);
		this.startTime = startTime;
		this.endTime = endTime;
		this.dayOfWeek = dayOfWeek;
	}

	private LocalDate getNextDate() {
		LocalDate date = LocalDate.now();
		while(!date.getDayOfWeek().equals(this.dayOfWeek)) {
			date = date.plusDays(1);
		}
		return date;
	}

	@Override
	OffsetDateTime getStartDateTime() {
		OffsetTime time = OffsetTime.of(this.startTime, getCurrentOffset());
		OffsetDateTime start = time.atDate(getNextDate());
		if(start.isBefore(OffsetDateTime.now())) start = start.plusWeeks(1);
		return start;
	}

	@Override
	OffsetDateTime getEndDateTime() {
		OffsetTime time = OffsetTime.of(this.endTime, getCurrentOffset());
		OffsetDateTime end = time.atDate(getNextDate());
		if(end.isBefore(OffsetDateTime.now())) end = end.plusWeeks(1);
		return end;
	}

	private ZoneOffset getCurrentOffset() {
		return ZoneId.systemDefault().getRules().getOffset(Instant.now());
	}

}
