/*
 *     Copyright (C) 2023  Jack Barter
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.jackb.pickler.events;

import dev.jackb.pickler.Pickler;
import net.dv8tion.jda.api.entities.ScheduledEvent;

import java.time.OffsetDateTime;
import java.util.stream.Collectors;

public abstract class ClubEvent {

	private final String displayName;
	private final String location;

	public ClubEvent(String displayName, String location) {
		this.displayName = displayName;
		this.location = location;
	}

	boolean isCreated() {
		return Pickler.getGuild().getScheduledEventsByName(this.displayName, false).stream()
				.anyMatch(e -> e.getStatus() == ScheduledEvent.Status.ACTIVE
						|| e.getStatus() == ScheduledEvent.Status.SCHEDULED);
	}

	String getDisplayName() {
		return displayName;
	}

	String getLocation() {
		return location;
	}

	abstract OffsetDateTime getStartDateTime();

	abstract OffsetDateTime getEndDateTime();

}
