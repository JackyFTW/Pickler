/*
 *     Copyright (C) 2023  Jack Barter
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.jackb.pickler.events;

import dev.jackb.liteprocess.util.TimerUtil;
import dev.jackb.pickler.Pickler;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class EventsManager {

	private static final List<ClubEvent> events = new ArrayList<>();

	static {
		events.add(new RecurringClubEvent("Triweekly Session (Mon)", "Rear Tennis Courts",
				LocalTime.of(18, 0, 0, 0),
				LocalTime.of(20, 30, 0, 0),
				DayOfWeek.MONDAY));
		events.add(new RecurringClubEvent("Triweekly Session (Wed)", "Rear Tennis Courts",
				LocalTime.of(18, 0, 0, 0),
				LocalTime.of(20, 30, 0, 0),
				DayOfWeek.WEDNESDAY));
		events.add(new RecurringClubEvent("Triweekly Session (Fri)", "Rear Tennis Courts",
				LocalTime.of(18, 0, 0, 0),
				LocalTime.of(20, 30, 0, 0),
				DayOfWeek.FRIDAY));
	}

	public static void init() {
		TimerUtil.schedule(() -> {
			for(ClubEvent e : events) {
				if(e.isCreated()) continue;

				Pickler.getGuild().createScheduledEvent(e.getDisplayName(), e.getLocation(),
						e.getStartDateTime(), e.getEndDateTime()).complete();
			}
		}, 0, Duration.ofHours(3).toMillis());
	}

}
